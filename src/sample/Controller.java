package sample;

import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.SubScene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.Rectangle;

public class Controller {

    @FXML private Pane clock;

    public void initClock() {

        PerspectiveCamera camera = new PerspectiveCamera();
        camera.setTranslateX(-150);
        camera.setTranslateY(-32.5);
        camera.setTranslateZ(-600);

        Group group = new Group();

        SubScene subScene = new SubScene(group, 300, 65);
        subScene.setCamera(camera);

        Rectangle rectangle = new Rectangle();
        rectangle.setArcWidth(1);
        rectangle.setArcHeight(1);
        rectangle.setX(0);
        rectangle.setY(0);
        rectangle.setWidth(300);
        rectangle.setHeight(65);

        Cylinder cylinder = new Cylinder(600, 65);
        PhongMaterial material = new PhongMaterial();

        Image diffuseMap
                = new Image(getClass()
                .getResource("/images/clock.png")
                .toExternalForm());

        material.setDiffuseColor(new Color(1, 1, 1, 1));
        material.setDiffuseMap(diffuseMap);
        cylinder.setMaterial(material);

        PointLight light = new PointLight();
        light.setColor(Color.WHITE);
        light.setTranslateX(0);
        light.setTranslateY(50);
        light.setTranslateZ(-800);

        group.getChildren().addAll(cylinder, light);

        clock.getChildren().add(subScene);
    }
}
